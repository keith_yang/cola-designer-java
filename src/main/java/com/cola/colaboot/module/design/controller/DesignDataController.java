package com.cola.colaboot.module.design.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.cola.colaboot.config.dto.Res;
import com.cola.colaboot.module.design.pojo.DesignData;
import com.cola.colaboot.module.design.service.DesignDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/design")
public class DesignDataController {

    @Autowired
    private DesignDataService designDataService;

    /**
     *  获取大屏详情
     * @param id    大屏ID
     * @param mode  0编辑，1发布浏览，2预览
     * @param viewCode  访问码
     */
    @GetMapping("/getById/{id}/{mode}/{viewCode}")
    public Res<?> getById(@PathVariable("id") String id, @PathVariable("mode") Integer mode,
                          @PathVariable("viewCode") String viewCode){
        DesignData design = designDataService.getById(id);
        if (mode == null || mode == 1){//浏览状态
            if (StringUtils.isNotBlank(design.getViewCode()) && !viewCode.equals(design.getViewCode())){
                return Res.ok("success","NEED_AUTH");
            }
        }
        return Res.ok(design);
    }

    @GetMapping("/authViewCode")
    public Res<?> authViewCode(String id, String viewCode){
        DesignData design = designDataService.getById(id);
        if (viewCode.equals(design.getViewCode())){
            return Res.ok(design);
        }
        return Res.fail("访问码错误");
    }

    @GetMapping("/pageList")
    public Res<?> queryPageList(DesignData designData,
                    @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                    @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {
        return Res.ok(designDataService.pageList(designData,pageNo,pageSize));
    }

    @PostMapping("/saveOrUpdate")
    public Res<?> saveOrUpdate(@RequestBody DesignData designData){
        designDataService.saveOrUpdate(designData);
        return Res.ok("保存成功");
    }

    @DeleteMapping("/delete")
    public Res<?> delete(String id){
        DesignData design = designDataService.getById(id);
        design.setState(-1);
        designDataService.updateById(design);
        return Res.ok();
    }
}
