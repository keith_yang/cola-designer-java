package com.cola.colaboot.module.design.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName("design_img_pool")
public class DesignImgPool {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String imgName;
    private String filePath;
    private String groupId;

    private String createUser;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private Integer countStar;

    @TableField(exist = false)
    private String groupName;
    @TableField(exist = false)
    private String username;
}
