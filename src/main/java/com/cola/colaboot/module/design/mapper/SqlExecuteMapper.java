package com.cola.colaboot.module.design.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface SqlExecuteMapper {

    @Select("${sqlStr}")
    Object execute(@Param(value = "sqlStr") String sqlStr);
}
