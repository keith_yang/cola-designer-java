package com.cola.colaboot.module.design.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cola.colaboot.module.design.pojo.DesignData;

public interface DesignDataService extends IService<DesignData> {
    IPage<DesignData> pageList(DesignData designData, Integer pageNo, Integer pageSize);
}
