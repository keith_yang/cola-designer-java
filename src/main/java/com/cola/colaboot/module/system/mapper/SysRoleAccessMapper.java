package com.cola.colaboot.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cola.colaboot.module.system.pojo.SysRoleAccess;
import org.apache.ibatis.annotations.Delete;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleAccessMapper extends BaseMapper<SysRoleAccess> {
    @Delete("delete from sys_role_access where access_id = #{accessId}")
    void delByAccessId(String accessId);
}
