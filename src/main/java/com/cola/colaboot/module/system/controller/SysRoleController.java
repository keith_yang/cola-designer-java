package com.cola.colaboot.module.system.controller;

import com.cola.colaboot.config.dto.Res;
import com.cola.colaboot.module.system.pojo.SysRole;
import com.cola.colaboot.module.system.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    @Autowired
    private SysRoleService roleService;

    @GetMapping("/pageList")
    public Res<?> queryPageList(SysRole sysRole,
                @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {
        return Res.ok(roleService.pageList(sysRole,pageNo,pageSize));
    }

    @PostMapping("/saveOrUpdate")
    public Res<?> saveOrUpdate(@RequestBody SysRole sysRole){
        roleService.saveOrUpdate(sysRole);
        return Res.ok();
    }
}
